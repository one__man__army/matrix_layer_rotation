#include <cstdio>
#include <iostream>
using namespace std;


// Printing out table
void wypisz_tablice(int *tab, int &r, int &k, int&szer)			
{
	for (int ri = 0; ri < r; ri++){
		for (int ki = 0; ki < k; ki++){
			cout << tab[ri*szer + ki];
		}
		cout << endl;
	}
}

// Create table that will define depth of each record
void stworz_obw(int *obw, int &r, int &k) {
	int minimum_rk = 0;
	if (r < k)
		minimum_rk = r;
	else
		minimum_rk = k;

	for (int ri = 0; ri < r; ri++){					// for rows
		for (int ki = 0; ki < k; ki++){				// for columns
			for (int obwi = 0; obwi < (minimum_rk / 2); obwi++){		// I will define depth
				if (((ki == obwi || ki == (k - 1 - obwi)) && ri >= obwi && ri <= (r - obwi - 1)) ||		// Condition for being in the given layer
					((ri == obwi || ri == (r - 1 - obwi)) && ki >= obwi && ki <= (k - obwi - 1))){		// calculating euclidean distance to the edge of matrix
					obw[ri * 300 + ki] = obwi;
				}
			}
		}
	}
}

// Code how the position of each cell in the matrix changes when we want to rotate it (left)
// for every layer and 2 dimensions
void stworz_tabele_pr_i_pk(int **pr, int **pk, int &r, int &k){
	int minimum_rk = 0;
	if (r < k)
		minimum_rk = r;
	else
		minimum_rk = k;

	for (int obwi = 0; obwi < (minimum_rk/2); obwi++){		// depth of the layer
		for (int x = 0; x < (2 * k + 2 * r - 4 - 8 * obwi); x++) {	// distance to the starting point (0,0)
			if (x >= 0 && x <= (r - 1 - 2 * obwi)){		// left side
				pr[obwi][x] = x;						// rows' index is increasing
				pk[obwi][x] = 0;						// while columns' stay the same
			}
			else{
				if (x >= (r - 2 * obwi) && x <= (r + k - 2 - 4 * obwi)){	// lower side
					pr[obwi][x] = r - 1 - 2 * obwi;							// rows are at max
					pk[obwi][x] = x - (r - 1 - 2 * obwi);					// columns are increasing
				}
				else{
					if (x >= (r + k - 1 - 4 * obwi) && x <= (2 * r + k - 3 - 6 * obwi)){		// right side
						pr[obwi][x] = r - 1 - 2 * obwi - (x - (r + k - 2 - 4 * obwi));			// rows' index in decreasing
						pk[obwi][x] = k - 1 - 2 * obwi;											// columns' index is constant
					}
					else{																//	upper side
						pr[obwi][x] = 0;												//  rows' index is equal 0
						pk[obwi][x] = k - 1 - 2 * obwi - (x - (2 * r + k - 3 - 6 * obwi));	// columns index is decreasing
					}
				}
			}
		}
	}
}

void zwroc_x_dla_zad(int &x, int &ri, int &ki, int *obw, int &r, int &k){
	// For given values return position x relative to the starting point of the layer, when moving anticlockwise
	int obwi = obw[ri * 300 + ki];		
	int pk = ki - obwi;		// Normalize system to start in position(0,0)
	int pr = ri - obwi;
	if (pk == 0){			// In the first column 
		x = pr;				// x is increasing with row index
	}
	else{			
		if (pr == r - 1 - 2 * obwi){			// In the lower row 
			x = r - 1 - 2 * obwi + pk;			// x is increasing with column index + value of down-left index
		}
		else{
			if (pk == k - 1 - 2 * obwi){		// In the right column
				x = 2 * r + k - 3 - 6 * obwi - pr;		// x is increasing when row index is decreasing
			}
			else{
				if (pr == 0){					// In the top row 
					x = (r + k) * 2 - 4 - 8 * obwi - pk;		// x is increasing when column index is decreasing 
				}
			}
		}
	}
}


int main() {
	// Read data
	int r = 7, k = 6, obr = 22;		// rows, columns, rotations
	int max_szer = 300;

	cin >> r >> k >> obr;
	int tab[300][300];
	int *tab_wsk = &tab[0][0];
	for (int ri = 0; ri < r; ri++){
		for (int ki = 0; ki < k; ki++){
			cin >> tab[ri][ki];
		}
	}
	int obw[300][300];
	int minimum_rk;
	if (r < k)
		minimum_rk = r;
	else
		minimum_rk = k;

	int* obw_wsk = &obw[0][0];
	stworz_obw(obw_wsk, r, k);
	int szer_prk = (k + r - 2) * 2;
	int **pr_wsk = new int*[minimum_rk];	// New pointers table
	int **pk_wsk = new int*[minimum_rk];
	for (int i = 0; i < minimum_rk; ++i){		// initialization in the loop
		pr_wsk[i] = new int[szer_prk];
		pk_wsk[i] = new int[szer_prk];
	}

	int **odpow_wsk = new int*[r];			// table for results
	for (int i = 0; i < r; i++){
		odpow_wsk[i] = new int[k];
	}

	int temp_szer = 22;
	stworz_tabele_pr_i_pk(pr_wsk, pk_wsk, r, k);
	int xx;
	int ri = 1;
	int ki = 0;
	int obwi = 0;
	int pos_r, pos_k;
	int pos_end_xx = 0;  // Final position (in x) for given index
	int l_okr = 0;		 // Length of the layer circle 
	for (int ri = 0; ri < r; ri++){
		for (int ki = 0; ki < k; ki++){
			zwroc_x_dla_zad(xx, ri, ki, obw_wsk, r, k);				// returning x- position on layer on which I am moving
			obwi = obw[ri][ki];										// obwi - depth of the layer
			l_okr = (r + k) * 2 - 4 - 8 * obwi;						// l_okr - circumference of the layer
			pos_end_xx = (xx + obr) % l_okr;						// Calculating final position for repeating rotation given number of times
			pos_r = pr_wsk[obwi][pos_end_xx];						// I find on what position (r,k) is x in the given layer
			pos_k = pk_wsk[obwi][pos_end_xx];						// 
			odpow_wsk[pos_r + obwi][pos_k + obwi] = tab[ri][ki];		// Solution: I put one cell of the input matrix into precalculated position of the output matrix
		}
	}
	for (int ri = 0; ri < r; ri++){
		for (int ki = 0; ki < k; ki++){
			cout << odpow_wsk[ri][ki] << " ";
		}
		cout << endl;
	}

	return 0;
	}