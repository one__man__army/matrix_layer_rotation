# README #

Problem statement:
https://www.hackerrank.com/challenges/matrix-rotation-algo/problem

### Solution report ###

* C++
* Precalculating final position with my own formulas.
* Reusing code
* Space Complexity: O(n)
* Time Complexity: O(n^2)

### Libraries used ###

* cstdio
* iostream

### Scoring ###

* Difficulty: Hard
* Max Score: 80
* Score: 80

### How to run ###
* Copy and paste solution into HackerRank 'current bufor', the submit. 